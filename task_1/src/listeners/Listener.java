package listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import java.util.Map;
import java.util.logging.Logger;

@WebListener
public class Listener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(Listener.class.getName());

    public Listener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();

        LOGGER.info(context.getServerInfo());
        LOGGER.info(context.getServletContextName());

        Map<String, ? extends ServletRegistration> registrations = context.getServletRegistrations();
        LOGGER.info("All available servlets ");
        for (Map.Entry<String, ? extends ServletRegistration> entry : registrations.entrySet()){
            LOGGER.info(entry.getKey() + " = " + entry.getValue().getName());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
