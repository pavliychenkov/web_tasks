package model.factories;

import model.dao.BookDAOJDBC;
import model.interfaces.IBookDAO;

import java.util.HashMap;
import java.util.Map;

public class BookDAOFactory {
    private static Map<String, IBookDAO> mapBookDAO = new HashMap<>();

    static {
        mapBookDAO.put(BookDAOJDBC.class.getName(), new BookDAOJDBC());
    }

    public static IBookDAO getBookDAO(String nameDAO){
        return mapBookDAO.get(nameDAO);
    }
}
