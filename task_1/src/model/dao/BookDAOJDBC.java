package model.dao;

import model.beans.Book;
import model.interfaces.IBookDAO;
import model.service.BDConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookDAOJDBC implements IBookDAO {
    Logger LOGGER = Logger.getLogger(BookDAOJDBC.class.getName());

    @Override
    public List<Book> getListBooks() {
        List<Book> books = new ArrayList<>();
        String query = "select * from tasksshema.books";
        BDConnection bdConnection = new BDConnection();

        try (PreparedStatement ps = bdConnection.getStatement(query);
                ResultSet rs = ps.executeQuery()){
            while (rs.next()){
                Integer id = rs.getInt(Book.ID);
                String name = rs.getString(Book.NAME);
                String author = rs.getString(Book.AUTHOR);
                String description = rs.getString(Book.DESCRIPTION);
                books.add(  new Book(id, name, author, description) );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return books;
    }
}