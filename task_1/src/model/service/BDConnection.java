package model.service;

import model.Constants;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BDConnection {
    Logger LOGGER = Logger.getLogger(BDConnection.class.getName());
    private Connection connection;
    private PreparedStatement statement;

    public BDConnection() {
        createConnection();
    }

    private Connection createConnection(){
        try {
            Class.forName(Constants.BD_CLASS_NAME);
            connection = DriverManager.getConnection(Constants. DB_URL,Constants.DB_USER, Constants.DB_PSWRD);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public PreparedStatement getStatement(String sql) throws SQLException {
        return statement = connection.prepareStatement(sql);
    }

    public void closeConnection(){
            try {                            
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
            }
        }
}
