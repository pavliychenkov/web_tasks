package model;

import model.dao.BookDAOJDBC;

public class Constants {
    //DAO
    public static final String BOOK_DAO = BookDAOJDBC.class.getName();

    //keys
    public static final String KEY_ACTION = "action";
    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_BOOKS = "books";

    //servlet names
    public static final String SERVLET_NAME_BASIC = "Basic Servlet";

    //servlet values
    public static final String SERVLET_VALUE_BASIC = "/getBooksController";

    //patterns
    public static final String SERVLET_URL_PATTERN_UNAVAILABILITY = "/unavailability/";

    //settings
    public static boolean FLAG_UNAVAILABILITY = true;                 //permanent or some time unavailable
    public static boolean FLAG_AVAILABILITY_PERMANENT = false;         //permanent or some time unavailable
    public static int     TIME_UNAVAILABLE = 3;                      //how long servlet unavailable
    //for bd connection
    public static final String BD_CLASS_NAME = "sun.jdbc.odbc.JdbcOdbcDriver";
    public static final String DB_URL = "jdbc:odbc:tasksConnector";
    public static final String DB_USER = "root";
    public static final String DB_PSWRD = "root";
    //some settings...

    //error msg's
    public static final String ERR_MSG_NOT_AVAILABLE_PERM = "This resource in not available permanently";
    public static final String ERR_MSG_NOT_AVAILABLE_TIME = "Resource will not is available still - ";

    //path's
    public static final String PATH = "/";
}
