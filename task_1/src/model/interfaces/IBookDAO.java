package model.interfaces;

import model.beans.Book;

import java.util.List;

public interface IBookDAO {
     List<Book> getListBooks();
}
