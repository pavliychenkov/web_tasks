package controllers;

import model.Constants;
import model.beans.Book;
import model.factories.BookDAOFactory;
import model.interfaces.IBookDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(
        value = Constants.SERVLET_VALUE_BASIC,
        name = Constants.SERVLET_NAME_BASIC,
        displayName = Constants.SERVLET_NAME_BASIC
)
public class BasicServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        showAllBooks(req,resp);
    }

    private void showAllBooks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        checkAvailable(request);

        IBookDAO bookDAO = BookDAOFactory.getBookDAO(Constants.BOOK_DAO);
        List<Book> books = bookDAO.getListBooks();

        request.setAttribute(Constants.KEY_BOOKS, books);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(Constants.PATH);
        requestDispatcher.forward(request,response);
    }

    protected void checkAvailable(HttpServletRequest request) throws UnavailableException {
        if (Constants.FLAG_UNAVAILABILITY) {
            if (request.getServletPath().equals(Constants.SERVLET_VALUE_BASIC)) {
                UnavailableException exception;
                if (Constants.FLAG_AVAILABILITY_PERMANENT) {
                    exception =  new UnavailableException(Constants.ERR_MSG_NOT_AVAILABLE_PERM);
                } else {
                    exception =  new UnavailableException(Constants.ERR_MSG_NOT_AVAILABLE_TIME + Constants.TIME_UNAVAILABLE , Constants.TIME_UNAVAILABLE);
                }
                Constants.FLAG_UNAVAILABILITY = false;
                throw exception;
            }
        }
    }
}
